#define M61_DISABLE 1
#include "m61.hh"
#include "hh.hh"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <assert.h>

#define assertf(x, msg, ...)                      \
    if (!(x)) {                                   \
        fprintf(stderr, msg "\n", ##__VA_ARGS__); \
        assert(x);                                \
    }

static m61_statistics gstats = { .nactive     = 0,
                                 .active_size = 0,
                                 .ntotal      = 0,
                                 .total_size  = 0,
                                 .nfail       = 0,
                                 .fail_size   = 0,
                                 .heap_min    = NULL,
                                 .heap_max    = NULL };

static const char MAGIC = '\0';

// NB: must be a multple of 16-bytes in size
struct alloc_header {
    alloc_header* selfp;
    const char* magic;
    size_t size;
    const char* file;
    long line;
    int64_t _pad;
};

#define TO_HEADER(p) (static_cast<alloc_header*>((void*)(p)))
#define HEADER_FOR(p) (TO_HEADER(p)-1)

#define NODELEN 31
typedef void* livenode[NODELEN+1];
static livenode livelist;

void marklive(void* p);
alloc_header* markfree(void* p);

/// m61_malloc(sz, file, line)
///    Return a pointer to `sz` bytes of newly-allocated dynamic memory.
///    The memory is not initialized. If `sz == 0`, then m61_malloc must
///    return a unique, newly-allocated pointer value. The allocation
///    request was at location `file`:`line`.

void* m61_malloc(size_t sz, const char* file, long line) {
    void* base = NULL;
    size_t sum;
    if (!__builtin_uaddl_overflow(sz, sizeof(alloc_header)+1, &sum)) {
        base = base_malloc(sum);
    }

    if (NULL == base) {
        gstats.nfail++;
        gstats.fail_size += sz;
        return NULL;
    }

    alloc_header* header = TO_HEADER(base);
    *header = { .selfp = header,
                .magic = &MAGIC,
                .size  = sz,
                .file  = file,
                .line  = line,
                ._pad  = -1 };
    char* ptr = static_cast<char*>(base) + sizeof(alloc_header);
    marklive(ptr);

    gstats.nactive++;
    gstats.active_size += sz;
    gstats.ntotal++;
    gstats.total_size += sz;
    if (NULL == gstats.heap_min) {
        gstats.heap_min = ptr;
    }
    if (ptr + sz > gstats.heap_max) {
        gstats.heap_max = ptr + sz;
    }
    ptr[sz] = -1;

    assertf(!(reinterpret_cast<uintptr_t>(ptr) & 0xF),
            "MEMORY BUG: %s:%li: pointer %p is not 16-byte aligned!",
            file, line, ptr);
    record_alloc(file, line, sz);
    return ptr;
}

void** findmark_from(livenode** nodep, int* ixp, void* ptr);

static inline void** findmark(void* ptr) {
    livenode* np = &livelist;
    int i = 0;
    return findmark_from(&np, &i, ptr);
}

void** findmark_from(livenode** nodep, int* ixp, void* ptr) {
    livenode* node = *nodep;
    int ix = *ixp;
    assertf(ix >= 0 && ix <= NODELEN, "findmark_from: index out of range! (%d)", ix);
    void** ret = NULL;
    void* closest = NULL;
    for (;; ix++) {
        void *p = (*node)[ix];
        if (NODELEN == ix) {
            if (NULL == p) {
                break;
            }
            node = static_cast<livenode*>(p);
            ix = 0;
            p = (*node)[ix];
        }
        if (p >= closest && p <= ptr) {
            closest = p;
            ret = &((*node)[ix]);
            if (closest == ptr)
                break;
        }
    }
    *nodep = static_cast<livenode*>((void*)node);
    *ixp = ix;
    return ret;
}

static livenode* curnode = &livelist;
static int nidx = 0;

void marklive(void* ptr) {
    void** mark = findmark_from(&curnode, &nidx, NULL);
    if (NULL == mark) {
        assertf(nidx == NODELEN, "togglemark: got NULL mark with bad nidx: %d", nidx);

        livenode* newnode = static_cast<livenode*>(base_malloc(sizeof(livenode)));
        memset(newnode, 0, sizeof(livenode));
        (*curnode)[nidx] = newnode;
        curnode = newnode;
        nidx = 0;
    }
    (*curnode)[nidx++] = ptr;
}

alloc_header* markfree(void* ptr) {
    livenode* cn = &livelist;
    int ix = 0;
    void** mark = findmark_from(&cn, &ix, ptr);
    if (NULL == mark || *mark != ptr) {
        return NULL;
    }
    *mark = NULL;
    curnode = cn;
    nidx = ix;
    return HEADER_FOR(ptr);
}

alloc_header* find_containing_header(void* ptr, int* offsetp) {
    void** mark = findmark(ptr);
    if (NULL == mark) {
        return NULL;
    }
    if (offsetp) *offsetp = static_cast<char*>(ptr) - static_cast<char*>(*mark);
    return HEADER_FOR(*mark);
}

/// m61_free(ptr, file, line)
///    Free the memory space pointed to by `ptr`, which must have been
///    returned by a previous call to m61_malloc. If `ptr == NULL`,
///    does nothing. The free was called at location `file`:`line`.

void m61_free(void* ptr, const char* file, long line) {
    if (!ptr) return;

    if (ptr < gstats.heap_min || ptr >= gstats.heap_max) {
        fprintf(stderr, "MEMORY BUG: %s:%li: invalid free of pointer %p, not in heap\n", file, line, ptr);
        abort();
    }
    alloc_header* header = markfree(ptr);
    if (NULL == header) {
        fprintf(stderr, "MEMORY BUG: %s:%li: invalid free of pointer %p, not allocated\n", file, line, ptr);
        int offset;
        header = find_containing_header(ptr, &offset);
        if (header) {
            fprintf(stderr, "  %s:%li: %p is %d bytes inside a %lu byte region allocated here\n",
                    header->file, header->line, ptr, offset, header->size);
        }
        abort();
    }
    if (static_cast<char*>(ptr)[header->size] != -1) {
        fprintf(stderr, "MEMORY BUG: detected wild write during free of pointer %p\n", ptr);
        abort();
    }
    gstats.nactive--;
    gstats.active_size -= header->size;
    base_free(header);
}


/// m61_calloc(nmemb, sz, file, line)
///    Return a pointer to newly-allocated dynamic memory big enough to
///    hold an array of `nmemb` elements of `sz` bytes each. If `sz == 0`,
///    then must return a unique, newly-allocated pointer value. Returned
///    memory should be initialized to zero. The allocation request was at
///    location `file`:`line`.

void* m61_calloc(size_t nmemb, size_t sz, const char* file, long line) {
    size_t prod = 0;
    if (__builtin_umull_overflow(nmemb, sz, &prod)) {
        gstats.nfail++;
        gstats.fail_size += prod;
        return nullptr;
    }
    void* ptr = m61_malloc(prod, file, line);
    if (ptr) {
        memset(ptr, 0, prod);
    }
    return ptr;
}


/// m61_getstatistics(stats)
///    Store the current memory statistics in `*stats`.

void m61_getstatistics(m61_statistics* stats) {
    *stats = gstats;
}


/// m61_printstatistics()
///    Print the current memory statistics.

void m61_printstatistics() {
    m61_statistics stats;
    m61_getstatistics(&stats);

    printf("alloc count: active %10llu   total %10llu   fail %10llu\n",
           stats.nactive, stats.ntotal, stats.nfail);
    printf("alloc size:  active %10llu   total %10llu   fail %10llu\n",
           stats.active_size, stats.total_size, stats.fail_size);
}


/// m61_printleakreport()
///    Print a report of all currently-active allocated blocks of dynamic
///    memory.

void m61_printleakreport() {
    if (NULL == gstats.heap_min) return;
    livenode* cn = &livelist;
    int ix = 0;
    for (;; ix++) {
        void* p = (*cn)[ix];
        if (NODELEN == ix) {
            if (NULL == p) break;
            cn = static_cast<livenode*>(p);
            ix = 0;
            p = (*cn)[0];
        }
        if (p) {
            alloc_header* header = HEADER_FOR(p);
            assertf(header->selfp == header,
                    "Found alloc with invalid selfp: (header = %p, selfp = %p)",
                    header, header->selfp);
            assertf(header->magic == &MAGIC,
                    "Found alloc with invalid magic: (&MAGIC = %p, magic = %p)",
                    &MAGIC, header->magic);
            printf("LEAK CHECK: %s:%li: allocated object %p with size %lu\n",
                   header->file, header->line, header->selfp+1, header->size);
        }
    }
}


thread_local const char* m61_file = "?";
thread_local int m61_line = 1;

void* operator new(size_t sz) {
    return m61_malloc(sz, m61_file, m61_line);
}
void* operator new[](size_t sz) {
    return m61_malloc(sz, m61_file, m61_line);
}
void operator delete(void* ptr) noexcept {
    m61_free(ptr, m61_file, m61_line);
}
void operator delete(void* ptr, size_t) noexcept {
    m61_free(ptr, m61_file, m61_line);
}
void operator delete[](void* ptr) noexcept {
    m61_free(ptr, m61_file, m61_line);
}
void operator delete[](void* ptr, size_t) noexcept {
    m61_free(ptr, m61_file, m61_line);
}

/// Debug printing

void printheader(alloc_header* header);

/// m61_dumpheap()
///    Print heap statistics
void m61_dumpheap() {
    fprintf(stderr, "+++ MAGIC: %p +++\n\n", &MAGIC);
    char* p = gstats.heap_min;
    alloc_header* header = HEADER_FOR(p);
    printheader(header);
    while (p < gstats.heap_max) {
        header = HEADER_FOR(p);
        if (header->selfp == header && header->magic == &MAGIC) {
            printheader(header);
            p += sizeof(alloc_header);
            p += header->size;
            uintptr_t pbits = reinterpret_cast<uintptr_t>(p);
            if (pbits & 0xF) {
                pbits >>= 4;
                ++pbits <<= 4;
                p = reinterpret_cast<char*>(pbits);
            }
        } else {
            p += 16;
        }
    }
}

void printheader(alloc_header* header) {
    fprintf(stderr,
            "=== Header: %p ===\n"
            "\tselfp  = %p\n"
            "\tmagic  = %p\n"
            "\tsize   = %lu\n"
            "\tfile   = %p\n"
            "\tline   = %li\n"
            "==================\n\n",
            header, header->selfp, header->magic, header->size,
            header->file, header->line);
}

/// Heavy hitters

static hitter_node hitlist;

typedef bool (*hiterator_fn)(heavy_hitter* hitter, void* state);

hitter_node* foreach_hitter(hiterator_fn fn, void* state) {
    hitter_node* node = &hitlist;
    int ix = 0;
    for(;; ix++) {
        if (HITTERLEN == ix) {
            if (NULL == node->next)
                break;
            node = node->next;
            ix = 0;
        }
        if (!fn(&node->hitters[ix], state))
            break;
    }
    return node;
}

struct hit_finder_state {
    const char* file;
    long line;
    heavy_hitter* hitter;
};

bool hitfinder(heavy_hitter* hitter, void* state) {
    hit_finder_state* st = static_cast<hit_finder_state*>(state);
    if (NULL == hitter->file)
        st->hitter = hitter;
    else if (!strcmp(hitter->file, st->file) && hitter->line == st->line)
        st->hitter = hitter;
    return st->hitter == NULL;
}

void record_alloc(const char* file, long line, size_t sz) {
    if (drand48() > 0.01) return; // sample 1%

    hit_finder_state st = { .file   = file,
                            .line   = line,
                            .hitter = NULL };
    hitter_node* node = foreach_hitter(hitfinder, &st);
    heavy_hitter* hit = st.hitter;
    assertf(node != NULL, "no heavy hitter node returned!");
    if (NULL == hit) {
        node->next = static_cast<hitter_node*>(base_malloc(sizeof(hitter_node)));
        memset(node->next, 0, sizeof(hitter_node));
        node = node->next;
        hit = &node->hitters[0];
    }
    if (NULL == hit->file) {
        hit->file = file;
        hit->line = line;
    }
    hit->bytes += sz;
    hit->count++;
}

bool summer(heavy_hitter* hitter, void* state) {
    if (NULL == hitter->file) return false;
    *static_cast<size_t*>(state) += hitter->bytes;
    return true;
}

bool print_heavy(heavy_hitter* hitter, void* state) {
    if (NULL == hitter->file) return false;
    size_t total = *static_cast<size_t*>(state);
    double pct = (double)hitter->bytes / (double)total;
    if (pct >= 0.1) {
        printf("HEAVY HITTER: %s:%li: %lu bytes (~%.1f%%)\n",
               hitter->file, hitter->line, hitter->bytes, pct*100);
        return true;
    }
    return false;
}

void sort_hitters() {
    hitter_node* node = &hitlist;
    int ix = 0;
    for (;; ix++) {
        if (HITTERLEN == ix) {
            if (NULL == node->next)
                break;
            node = node->next;
            ix = 0;
        }
        heavy_hitter* hitter = &node->hitters[ix];
        if (NULL == hitter->file)
            break;
        heavy_hitter* max = hitter;

        int jx = ix+1;
        hitter_node* node0 = node;
        for (;; jx++) {
            if (HITTERLEN == jx) {
                if (NULL == node0->next)
                    break;
                node0 = node0->next;
                jx = 0;
            }
            heavy_hitter* hitter0 = &node0->hitters[jx];
            if (NULL == hitter0->file)
                break;
            if (hitter0->bytes > max->bytes)
                max = hitter0;
        }
        if (hitter != max) {
            heavy_hitter temp = *hitter;
            *hitter = *max;
            *max = temp;
        }
    }
}

void print_hitters() {
    sort_hitters();
    size_t total_bytes = 0;
    foreach_hitter(summer, &total_bytes);
    foreach_hitter(print_heavy, &total_bytes);
}
