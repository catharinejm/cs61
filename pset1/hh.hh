struct heavy_hitter {
    const char* file;
    long line;
    size_t bytes;
    long count;
};

#define HITTERLEN 7

struct hitter_node {
    heavy_hitter hitters[HITTERLEN];
    hitter_node* next;
    int64_t _pad[3];
};

void record_alloc(const char* file, long line, size_t sz);
void print_hitters();
